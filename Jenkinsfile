pipeline
{
    options
    {
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
    }
    agent any
    environment 
    {
		VERSION = 'latest'
        PROJECT = 'salsify-productdataservice-images'
        IMAGE = 'salsify-productdataservice-images:latest'
        ECRURL = 'https://298789437745.dkr.ecr.us-east-2.amazonaws.com/salsify-productdataservice-images'
        ECRCRED = 'ecr:us-east-2:aws-ecr-credential'
    }
    stages
    {
        stage('SCM Checkout Repository'){
            steps{
                script{
					echo "Begin: Clone bitbucket repository.."
			        git credentialsId: 'bitbucket-credentials', url: 'https://rajalingamrajendran@bitbucket.org/rajalingamrajendran/product-data-service.git', branch: 'master'
					echo "End: Clone bitbucket repository.."
                }
            }
		}

        stage('Build Preparations')
        {
            steps
            {
                script 
                {
					echo "Begin: Preparing build process.."
                    // calculate GIT lastest commit short-hash
                    gitCommitHash = bat(returnStdout: true, script: '@echo off \n git rev-parse HEAD').trim()
                    shortCommitHash = gitCommitHash.take(7)
                    
                    // calculate a sample version tag
                    VERSION = shortCommitHash
					
                    // set the build display name
                    currentBuild.displayName = "#${BUILD_ID}-${VERSION}"
                    IMAGE = "$PROJECT:$VERSION" 
					echo "End: Preparing build process.."
                }
            }
        }
        
        stage('Maven Sprint Boot Build'){
			steps{
                script{
					echo "Begin: Packaging build artifacts.."
					def mvnHome = tool name: 'apache-maven-3.6.2', type: 'maven'
					def mvnCMD = "${mvnHome}/bin/mvn"
					bat "${mvnCMD} clean package"
					echo "End: Packaging build artifacts.."
				}
			}
		}
        
        stage('Compress Build Artifacts'){
			steps{
                script{		
					echo "Begin: Zipping build artifacts.."
					zip dir: 'target', glob: '*.jar', zipFile: "target/product-data-service-${BUILD_ID}-${shortCommitHash}.zip"
					echo "End: Zipping build artifacts.."
				}
			}
		}
		
		stage('Build Docker Image'){
			steps{
				script{
					echo "Begin: Building docker image.."
					docker.build("$IMAGE",".")
					echo "End: Building docker image.."
				}
			}
		}
		
		stage('Push Docker Image to AWS ECR'){
			steps{
				script{
					echo "Begin: Pushing docker image to AWS ECR.."
					docker.withRegistry("$ECRURL", ECRCRED) {
					    docker.image(PROJECT).push(VERSION)
					}
					echo "End: Pushing docker image to AWS ECR.."
				}
			}
		}
		
		stage('Publish Build Artifacts to AWS S3 Bucket'){
			steps{				
				script{		
					echo "Begin: Publishing build artifacts to AWS S3 Bucket.."
					s3Upload consoleLogLevel: 'INFO', dontWaitForConcurrentBuildCompletion: false, entries: [[bucket: 'salsify-product-data-service-bucket', excludedFile: '', flatten: false, gzipFiles: false, keepForever: false, managedArtifacts: false, noUploadOnFailure: false, selectedRegion: 'ap-south-1', showDirectlyInBrowser: false, sourceFile: 'target/*.zip', storageClass: 'STANDARD', uploadFromSlave: false, useServerSideEncryption: false]], pluginFailureResultConstraint: 'FAILURE', profileName: 'salsify-aws-s3-profile', userMetadata: []
					echo "End: Publishing build artifacts to AWS S3 Bucket.."
				}
			}
		}
    }
    
    post
    {
        always
        {
			echo "Begin: Removing locally build docker image.."
            bat "docker rmi $IMAGE"
			echo "End: Removing locally build docker image.."
        }
    }
}