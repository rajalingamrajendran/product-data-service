FROM openjdk:8

ADD target/productdataservice-0.0.1-SNAPSHOT.jar productdataservice.jar

ENTRYPOINT ["java", "-jar", "productdataservice.jar"]

EXPOSE 8046